package ProyectoM5;

import java.util.ArrayList;
import java.util.Scanner;

public class ChampPool {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		for (int i = 0; i < casos; i++) {
			String personatges = "";
			int qTipus = sc.nextInt();
			sc.nextLine();
			for (int j = 0; j < qTipus; j++) {
				String nomFitxa = sc.next();
				int qFitxa = sc.nextInt();
				sc.nextLine();
				for (int k = 0; k < qFitxa; k++) {
					if (personatges.equals("")) {
						personatges = nomFitxa;
					}else {
						personatges = personatges + "," + nomFitxa;
					}
					
				}
				
			}
			
			String buscar = sc.nextLine();
			String quantitat = cosasDelTFT(buscar, personatges.toLowerCase());
			System.out.println(quantitat);			
			
		}
	}
	public static String cosasDelTFT(String buscar, String personatges){
		if (buscar.equals("")) {
			return "No pots demanar un linia buida";
		}else {
		String[] split = personatges.split(",");
		boolean flag = false;
		for(int i=0;i<split.length;i++) {
			if(split[i].equals(buscar.toLowerCase())) {
				flag = true;
			}
		}
			if (flag) {
				int cont = 0;
				for (int j = 0; j < split.length; j++) {
					if (split[j].equals(buscar.toLowerCase())) {
						cont++;
					}
				}
				String resultat = "Encara queden " + cont + " " + buscar;
				return resultat;
				
			} else {
				String resultat = "No queden " + buscar;
				return resultat;
			}
		}	
	}

}
