package ProyectoM5;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

class ChampPoolTest {
	
	@Test
	void testPublic() {
		assertEquals("Encara queden 3 zed", ChampPool.cosasDelTFT("zed", "zed,zed,zed,ahri,ahri,viktor"));
		assertEquals("No queden viktor", ChampPool.cosasDelTFT("viktor", "zed,zed,ahri,ahri,vel'koz"));
		assertEquals("No pots demanar un linia buida", ChampPool.cosasDelTFT("", "zed,zed,ahri,ahri,viktor"));
	}
	@Test
	void testDificil() {
		assertEquals("Encara queden 2 Ahri", ChampPool.cosasDelTFT("Ahri", "zed,zed,ahri,ahri,viktor"));
		assertEquals("No queden zed", ChampPool.cosasDelTFT("zed", ""));
		assertEquals("No queden  ", ChampPool.cosasDelTFT(" ", ""));
		assertEquals("No queden zed ahri", ChampPool.cosasDelTFT("zed ahri", "zed,zed,ahri,ahri,viktor"));
	}

}
